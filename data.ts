import { Ability, Hero, Menu, Outpost, Tower } from "wrapper/Imports"
import { ParticlesX } from "X-Core/Imports"
import { IMenuParticlePicker } from "../wrapper/wrapper/Menu/ITypes"

export interface IRadiuesesMenu {
	NodeBase: Menu.Node
	Selector: Menu.ImageSelector
}

export interface BaseMenuSDKAbilitiesType {
	Hero: Hero
	Abilities: Ability[]
	Menu: IRadiuesesMenu
	CachedEnabled: Map<string, boolean>
}

export interface IMenuParticlePickerExtend extends IMenuParticlePicker {
	Team?: Menu.Dropdown
}

export interface IMenuParticlePickerCustomExtend extends IMenuParticlePicker {
	Radius: Menu.Slider
}

export interface BaseMenuSDKBulding {
	Team: Menu.Dropdown
	Width: Menu.Slider
	Sytle: Menu.Dropdown
	NodeBase: Menu.Node
	StateNode: Menu.Toggle
	AllyColor: Menu.ColorPicker
	EnemyColor: Menu.ColorPicker
}

export class RADIUSES_DATA {

	public static ParticlesSDK = new ParticlesX()
	public static RangeCache = new Map<Ability, number>()
	public static AttackRangeCache = new Map<Hero, number>()
	public static AttackMenuSDKCache = new Map<string, IMenuParticlePickerExtend>()
	public static NodeParticles = new Map<Ability, IMenuParticlePickerExtend>()
	public static BaseMenuSDKAbilities = new Map<string, BaseMenuSDKAbilitiesType>()

	public static DisposeAbilities(destroy: Ability) {
		this.RangeCache.delete(destroy)
		this.NodeParticles.delete(destroy)
		this.ParticlesSDK.DestroyByKey(this.AbilRadiusName(destroy))
	}

	public static DisposeHeroAbilities(destroy: Hero) {
		this.AttackRangeCache.delete(destroy)
		this.AttackMenuSDKCache.delete(destroy.Name)
		this.BaseMenuSDKAbilities.get(destroy.Name)?.Menu?.NodeBase?.DetachFromParent()
		this.BaseMenuSDKAbilities.delete(destroy.Name)
	}

	public static AttackName = (x: Tower) => `RADIUSES_ATTACK_${x.Index}`
	public static AbilRadiusName = (x: Ability) => `RADIUSES_ABILITIES_${x.Index}`
	public static AttackRadiusName = (x: Hero) => `RADIUSES_ATTACK_${x.Index}`
	public static BuildingRadiusName = (x: Tower | Outpost) => `RADIUSES_${x.Index}`

	public static Dispose() {
		this.ParticlesSDK.DestroyAll()
	}
}
