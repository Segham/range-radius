import { Outpost } from "wrapper/Imports"
import { EntityX } from "X-Core/Imports"
import { RADIUSES_PARTICLE_SERVICE } from "../../Base/Particles"
import { RADIUSES_DATA } from "../../data"
import { Outposts, State } from "../../menu"

function OnStateChanged() {
	RADIUSES_PARTICLE_SERVICE.OnStateChanged(Outposts, EntityX.OutPosts)
}

Outposts.Team.OnValue(OnStateChanged)
Outposts.Sytle.OnValue(OnStateChanged)
State.OnValue(OnStateChanged)
Outposts.StateNode.OnValue(OnStateChanged)
Outposts.Width.OnValue(OnStateChanged)
Outposts.AllyColor.OnValue(OnStateChanged)
Outposts.EnemyColor.OnValue(OnStateChanged)

export const RADIUSES_OUTPOST_DESTROY = (ent: Outpost) => {
	if (ent.IsAlive)
		return
	RADIUSES_DATA.ParticlesSDK.DestroyByKey(RADIUSES_DATA.BuildingRadiusName(ent))
}

export const RADIUSES_OUTPOST_CHANGED = OnStateChanged
