import { LocalPlayer, ParticleAttachment_t } from "wrapper/Imports"
import { RADIUSES_DATA } from "../../data"
import { CustomRadiusNode } from "../../menu"

export class RADIUSES_CUSTOM {
	public static OnChanged(node: CustomRadiusNode, state: boolean) {
		if (state && node.BaseNode.State!.value) {
			const hero = LocalPlayer?.Hero
			if (hero === undefined)
				return
			const color = node.BaseNode.Color.selected_color
			RADIUSES_DATA.ParticlesSDK.DrawCircle(node, hero, node.range.value, {
				Width: node.BaseNode.Width.value,
				Attachment: ParticleAttachment_t.PATTACH_ABSORIGIN_FOLLOW,
				Color: color,
				RenderStyle: node.BaseNode.Style.selected_id,
				Alpha: color.a,
			})
		} else
			RADIUSES_DATA.ParticlesSDK.DestroyByKey(node)
	}
}
