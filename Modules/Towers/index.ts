import { Tower } from "wrapper/Imports"
import { EntityX } from "X-Core/Imports"
import { RADIUSES_PARTICLE_SERVICE } from "../../Base/Particles"
import { RADIUSES_DATA } from "../../data"
import { State, Towers, TowersAttackColor, TowersAttackState, TowersAttackTeamState } from "../../menu"

function DestroyParticles(ent: Tower) {
	RADIUSES_DATA.ParticlesSDK.DestroyByKey(RADIUSES_DATA.BuildingRadiusName(ent))
}

function OnStateChanged() {
	RADIUSES_PARTICLE_SERVICE.OnStateChanged(Towers, [
		...EntityX.AllyTowers,
		...EntityX.EnemyTowers,
	])
}

Towers.Team.OnValue(OnStateChanged)
Towers.Sytle.OnValue(OnStateChanged)
State.OnValue(OnStateChanged)
Towers.StateNode.OnValue(OnStateChanged)

TowersAttackState.OnDeactivate(() => RADIUSES_PARTICLE_SERVICE.AttackTargetDestroy([
	...EntityX.AllyTowers,
	...EntityX.EnemyTowers,
]))

Towers.Width.OnValue(OnStateChanged)
Towers.AllyColor.OnValue(OnStateChanged)
Towers.EnemyColor.OnValue(OnStateChanged)

export const RADIUSES_TICK_TOWERS = () => {
	if (!State.value || !TowersAttackState.value)
		return

	const towers = [...EntityX.AllyTowers, ...EntityX.EnemyTowers]
	towers.forEach(tower => {
		const target = tower.TowerAttackTarget,
			name = RADIUSES_DATA.AttackName(tower)

		if (
			target !== undefined
			&& target.IsAlive
			&& tower.IsInRange(target.Position, tower.AttackRangeBonus())
			&& !RADIUSES_PARTICLE_SERVICE.Team(TowersAttackTeamState, tower)
		)
			RADIUSES_DATA.ParticlesSDK.DrawLineToTarget(name, tower, target, TowersAttackColor.selected_color)
		else
			RADIUSES_DATA.ParticlesSDK.DestroyByKey(name)
	})
}

export const RADIUSES_TOWER_DESTROY = (ent: Tower) =>
	DestroyParticles(ent)

export const RADIUSES_TOWER_LIFSTATE = (ent: Tower) => {
	if (!ent.IsAlive)
		DestroyParticles(ent)
}
