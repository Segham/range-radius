import { Ability, Hero } from "wrapper/Imports"
import { BaseMenuSDKAbilitiesType, RADIUSES_DATA } from "../../data"
import { CreateMenuRadiusHero, State } from "../../menu"
import { RADIUSES_PARTICLES } from "./Particles"

State.OnActivate(() => RADIUSES_PARTICLES.OnUpdateMenu())

export const RADIUSES_HERO_CREATED = (hero: Hero) => {

	const IsCreated = RADIUSES_DATA.BaseMenuSDKAbilities.get(hero.Name)

	if (IsCreated !== undefined) {
		IsCreated.Hero = hero
		return
	}

	const obj = CreateMenuRadiusHero(hero.Name)
	obj.NodeBase.save_unused_configs = true

	const menu: BaseMenuSDKAbilitiesType = {
		Hero: hero,
		Menu: obj,
		Abilities: [],
		CachedEnabled: new Map(),
	}

	obj.Selector.OnValue(caller => {
		caller.enabled_values.forEach((state, abilName) => {
			if (menu.CachedEnabled.get(abilName) !== state) {
				menu.CachedEnabled.set(abilName, state)
				const abil = menu.Abilities.find(x => x.Name === abilName)
				if (abil !== undefined)
					RADIUSES_PARTICLES.OnAbilityChanged(menu, abil)
			}
		})
	})

	RADIUSES_DATA.BaseMenuSDKAbilities.set(hero.Name, menu)
	RADIUSES_PARTICLES.OnUpdateMenu()
}

export const RADIUSES_ABILITY_UPDATED = (abil: Ability) => {
	if (!(abil.Owner instanceof Hero) || abil.Owner.IsIllusion || !RADIUSES_PARTICLES.Filter(abil))
		return
	RADIUSES_PARTICLES.OnUpdateMenu()
}

export const RADIUSES_ENTITY_LIFSTATE = (hero: Hero) => {
	if (hero.IsIllusion)
		return
	RADIUSES_PARTICLES.OnUpdateMenu()
}

export const RADIUSES_TICK_ABILITIES = () => RADIUSES_PARTICLES.OnTick()
export const RADIUSES_DRAW_ABILITIES = () => RADIUSES_PARTICLES.OnDraw()
