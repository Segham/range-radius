import { Ability, ArrayExtensions, beastmaster_mark_of_the_beast, Color, DOTA_ABILITY_BEHAVIOR, GameState, Hero, huskar_burning_spear, item_aether_lens, item_branches, item_clarity, item_enchanted_mango, item_flask, item_greater_mango, item_mango_tree, item_spy_gadget, item_tpscroll, item_travel_boots, item_travel_boots_2, item_ultimate_scepter, kunkka_tidebringer, life_stealer_open_wounds, nevermore_shadowraze1, nevermore_shadowraze2, nevermore_shadowraze3, ParticleAttachment_t, Team } from "wrapper/Imports"
import { IDrawCircleOptions } from "wrapper/wrapper/Managers/ParticleManager"
import { IMenuParticlePicker } from "wrapper/wrapper/Menu/ITypes"
import { PathX } from "X-Core/Imports"
import { BaseMenuSDKAbilitiesType, RADIUSES_DATA } from "../../data"
import { PARTICLE_NAME, State, TeamArray } from "../../menu"

export class RADIUSES_PARTICLES {

	public static OnAbilityChanged(
		menu: BaseMenuSDKAbilitiesType,
		abil: Ability,
		state = menu.CachedEnabled.get(abil.Name) ?? false,
	) {

		const abilMenu = RADIUSES_DATA.NodeParticles.get(abil)
		if (abilMenu === undefined)
			return

		abilMenu.Node.IsHidden = !state
		abilMenu.Node.icon_path = abil.IsItem
			? PathX.DOTAItems(abil.Name)
			: PathX.DOTAAbilities(abil.Name)

		const radiusName = RADIUSES_DATA.AbilRadiusName(abil)

		const owner = abil.Owner

		if (
			owner !== undefined
			&& State.value
			&& !(abilMenu.Team!.selected_id === 1 && !owner.IsEnemy())
			&& !(abilMenu.Team!.selected_id === 2 && owner.IsEnemy() && GameState.LocalTeam !== Team.Observer)
			&& owner.IsAlive
			&& state
		)
			RADIUSES_PARTICLES.AbilityCreate(abilMenu, abil)
		else
			RADIUSES_DATA.ParticlesSDK.DestroyByKey(radiusName)
	}

	public static OnAttackRangeChanged(menu: BaseMenuSDKAbilitiesType) {
		const AttackMenu = RADIUSES_DATA.AttackMenuSDKCache.get(menu.Hero.Name)
		if (AttackMenu === undefined)
			return

		const name = RADIUSES_DATA.AttackRadiusName(menu.Hero)

		if (
			State.value
			&& AttackMenu.State!.value
			&& !(AttackMenu.Team!.selected_id === 1 && !menu.Hero.IsEnemy())
			&& !(AttackMenu.Team!.selected_id === 2 && menu.Hero.IsEnemy() && GameState.LocalTeam !== Team.Observer)
			&& menu.Hero.IsAlive
		)
			RADIUSES_PARTICLES.AttackRangeCreate(AttackMenu, menu.Hero)
		else
			RADIUSES_DATA.ParticlesSDK.DestroyByKey(name)
	}

	public static OnUpdateMenu() {

		RADIUSES_DATA.BaseMenuSDKAbilities.forEach(menu => {

			let AttackMenuSDKCache = RADIUSES_DATA.AttackMenuSDKCache.get(menu.Hero.Name)
			if (AttackMenuSDKCache === undefined) {
				AttackMenuSDKCache = menu.Menu.NodeBase.AddParticlePicker("Attack", Color.Green, PARTICLE_NAME, [true, false])
				AttackMenuSDKCache.Node.icon_path = PathX.Images.icon_damage
				RADIUSES_DATA.AttackMenuSDKCache.set(menu.Hero.Name, AttackMenuSDKCache)
				AttackMenuSDKCache.Team = AttackMenuSDKCache.Node.AddDropdown("Team", TeamArray)
				AttackMenuSDKCache.Team.OnValue(() => this.OnAttackRangeChanged(menu))
				AttackMenuSDKCache.Style.OnValue(() => this.OnAttackRangeChanged(menu))
				AttackMenuSDKCache.Width.OnValue(() => this.OnAttackRangeChanged(menu))
				AttackMenuSDKCache.Color.OnValue(() => this.OnAttackRangeChanged(menu))
				AttackMenuSDKCache.State?.OnValue(() => this.OnAttackRangeChanged(menu))
			}

			const abilities = [...menu.Hero.Spells, ...menu.Hero.Items]
				.filter(x => RADIUSES_PARTICLES.Filter(x)) as Ability[]

			menu.Abilities.forEach(abil => {
				if (abilities.includes(abil))
					return

				const abilMenu = RADIUSES_DATA.NodeParticles.get(abil)
				if (abilMenu !== undefined) {
					abilMenu.Node.DetachFromParent()
					ArrayExtensions.arrayRemove(menu.Menu.Selector.values, abil.Name)
					this.OnAbilityChanged(menu, abil, false)
				}
			})
			this.OnAttackRangeChanged(menu)

			menu.Abilities = abilities
			menu.Abilities.forEach(abil => {

				if (menu.Menu.Selector.values.includes(abil.Name))
					return

				menu.Menu.Selector.values.push(abil.Name)
				menu.Menu.Selector.Update()

				let abilMenu = RADIUSES_DATA.NodeParticles.get(abil)
				if (abilMenu === undefined) {
					abilMenu = menu.Menu.NodeBase.AddParticlePicker(abil.Name, Color.Green, PARTICLE_NAME)
					RADIUSES_DATA.NodeParticles.set(abil, abilMenu)
					abilMenu.Team = abilMenu.Node.AddDropdown("Team", TeamArray)
					abilMenu.Color.OnValue(() => this.OnAbilityChanged(menu, abil))
					abilMenu.Width.OnValue(() => this.OnAbilityChanged(menu, abil))
					abilMenu.Style.OnValue(() => this.OnAbilityChanged(menu, abil))
					abilMenu.Team.OnValue(() => this.OnAbilityChanged(menu, abil))
				}

				abilMenu.Node.IsHidden = true
			})

			menu.Abilities.forEach(ability => this.OnAbilityChanged(menu, ability))
			menu.Menu.Selector.Update()
		})
	}

	public static AttackRangeCreate(menu: IMenuParticlePicker, hero: Hero) {
		const circleSettings: IDrawCircleOptions = {
			Attachment: ParticleAttachment_t.PATTACH_ABSORIGIN_FOLLOW,
			Width: menu.Width.value,
			Color: menu.Color.selected_color,
			RenderStyle: menu.Style.selected_id,
			Alpha: menu.Color.selected_color.a,
		}
		const name = RADIUSES_DATA.AttackRadiusName(hero)
		RADIUSES_DATA.ParticlesSDK.DrawCircle(
			name,
			hero,
			hero.AttackRangeBonus(),
			circleSettings,
		)
	}

	public static AbilityCreate(menu: IMenuParticlePicker, abil: Ability) {

		const owner = abil.Owner
		if (owner === undefined)
			return

		const circleSettings: IDrawCircleOptions = {
			Attachment: ParticleAttachment_t.PATTACH_ABSORIGIN_FOLLOW,
			Width: menu.Width.value,
			Color: menu.Color.selected_color,
			RenderStyle: menu.Style.selected_id,
			Alpha: menu.Color.selected_color.a,
		}

		let castRange = this.FilterRange(abil)
		if (abil instanceof nevermore_shadowraze1 || abil instanceof nevermore_shadowraze2 || abil instanceof nevermore_shadowraze3) {
			circleSettings.Attachment = ParticleAttachment_t.PATTACH_ABSORIGIN
			circleSettings.Position = owner.InFront(castRange)
			castRange = this.GetAbilityAOERadius(abil)
		}

		RADIUSES_DATA.ParticlesSDK.DrawCircle(RADIUSES_DATA.AbilRadiusName(abil), owner, castRange, circleSettings)
	}

	public static Filter(abil: Nullable<Ability>) {
		if (abil === undefined)
			return false

		const range = this.FilterRange(abil)
		if (range <= 0 || range >= 10000)
			return false

		return !(abil instanceof life_stealer_open_wounds)
			&& !(abil instanceof huskar_burning_spear)
			&& !(abil instanceof beastmaster_mark_of_the_beast)
			&& !(abil instanceof item_tpscroll)
			&& !(abil instanceof item_mango_tree)
			&& !(abil instanceof item_enchanted_mango)
			&& !(abil instanceof item_travel_boots)
			&& !(abil instanceof item_travel_boots_2)
			&& !(abil instanceof item_greater_mango)
			&& !(abil instanceof item_branches)
			&& !(abil instanceof item_clarity)
			&& !(abil instanceof item_flask)
			&& !(abil instanceof item_spy_gadget)
			&& !(abil instanceof item_aether_lens)
			&& !(abil instanceof item_ultimate_scepter)
			&& !abil.Name.includes("special_")
			&& !this.AbilitySpecialHidden.includes(abil.Name)
	}

	public static FilterRange(abil: Ability) {
		if (!(abil instanceof nevermore_shadowraze1 || abil instanceof nevermore_shadowraze2 || abil instanceof nevermore_shadowraze3)
			&& (abil.IsPassive || abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_NO_TARGET)))
			return this.GetAbilityAOERadius(abil)

		if (abil instanceof kunkka_tidebringer)
			return this.GetAbilityAOERadius(abil)

		return this.GetAbilityCastRange(abil)
	}

	public static OnTick() {
		RADIUSES_DATA.BaseMenuSDKAbilities.forEach(menu => {
			this.UpdateAttackRange(menu)
			this.UpdateAbilitesRange(menu)
		})
	}

	public static OnDraw() {
		if (!State.value)
			return

		RADIUSES_DATA.BaseMenuSDKAbilities.forEach(menu => {
			menu.Abilities.forEach(abil => {
				if (abil instanceof nevermore_shadowraze1 || abil instanceof nevermore_shadowraze2 || abil instanceof nevermore_shadowraze3) {
					const owner = abil.Owner
					if (owner === undefined)
						return
					RADIUSES_DATA.ParticlesSDK.SetConstrolPointByKey(
						RADIUSES_DATA.AbilRadiusName(abil),
						0,
						owner.InFront(this.FilterRange(abil)),
					)
				}
			})
		})
	}

	private static readonly AbilitySpecialHidden: string[] = [
		"high_five",
		"seasonal_ti9_banner",
		"morphling_morph_agi",
		"morphling_morph_str",
		"invoker_empty1",
		"invoker_empty2",
		"doom_bringer_empty1",
		"doom_bringer_empty2",
		"generic_hidden",
		"rubick_hidden1",
		"rubick_hidden2",
		"rubick_hidden3",
		"rubick_empty1",
		"rubick_empty2",
		"seasonal_ti10_",
		"invoker_invoke",
		"invoker_quas",
		"invoker_wex",
		"invoker_exort",
	]

	private static GetAbilityCastRange(abil: Ability): number {
		return abil.GetCastRangeForLevel(Math.max(abil.Level, 1))
	}

	private static GetAbilityAOERadius(abil: Ability): number {
		return abil.GetAOERadiusForLevel(Math.max(abil.Level, 1))
	}

	private static UpdateAttackRange(menu: BaseMenuSDKAbilitiesType) {
		const stateAttackRange = RADIUSES_DATA.AttackRangeCache.get(menu.Hero)
		if (stateAttackRange !== menu.Hero.AttackRangeBonus()) {
			RADIUSES_DATA.AttackRangeCache.set(menu.Hero, menu.Hero.AttackRangeBonus())
			this.OnAttackRangeChanged(menu)
		}
	}

	private static UpdateAbilitesRange(menu: BaseMenuSDKAbilitiesType) {
		menu.Abilities.forEach(abil => {
			const stateRange = RADIUSES_DATA.RangeCache.get(abil)
			if (stateRange !== this.FilterRange(abil)) {
				RADIUSES_DATA.RangeCache.set(abil, this.FilterRange(abil))
				this.OnAbilityChanged(menu, abil)
			}
		})
	}
}
