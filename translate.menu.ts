import { Menu } from "wrapper/Imports"

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["RADIUSES_X", ""],
	["Count", "Количество"],
	["Team", "Команда"],
	["Attack", "Атака"],
	["Towers", "Башни"],
	["Custom", "Кастомные"],
	["Radius", "Радиус"],
	["Radiuses", "Радиусы"],
	["Outposts", "Аванпосты"],
	["Color ally", "Цвет созных"],
	["Color enemy", "Цвет вражеских"],
	["Style radius", "Стиль радиуса"],
	["Only allies", "Только союзные"],
	["Width radius", "Ширина радиуса"],
	["Only enemy", "Только вражеские"],
	["Color radius", "Цвет радиуса"],
	["Allies and enemy", "Союзные и вражеские"],
	["Color ally radius towers", "Цвет союзных радиусов башен"],
	["Color enemy radius towers", "Цвет вражеских радиусов башен"],
	["Turn on/off tower radius", "Включить/Выключить радиус башен"],
	["Turn on/off attack target tower", "Включить/Выключить показ цели башни"],
]))

Menu.Localization.AddLocalizationUnit("english", new Map([
	["RADIUSES_X", ""],
]))
