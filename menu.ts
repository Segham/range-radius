import { Color, Menu as MenuSDK, PARTICLE_RENDER_NAME } from "wrapper/Imports"
import { PathX } from "X-Core/Imports"
import { IMenuParticlePicker } from "../wrapper/wrapper/Menu/ITypes"
import { RADIUSES_DATA } from "./data"
import { RADIUSES_CUSTOM } from "./Modules/Custom/index"

// Base menu
const Menu = MenuSDK.AddEntryDeep(["Visual", "Radiuses"])
export const State = Menu.AddToggle("State")
State.OnDeactivate(() => RADIUSES_DATA.Dispose())

export const TeamArray: string[] = [
	"Allies and enemy",
	"Only enemy",
	"Only allies",
]

export const PARTICLE_NAME: PARTICLE_RENDER_NAME[] = [
	PARTICLE_RENDER_NAME.NORMAL,
	PARTICLE_RENDER_NAME.ROPE,
	PARTICLE_RENDER_NAME.ANIMATION,
]

// Towers & Outposts
const CreateMenuTowerOrOutpost = (name: string) => {
	const NodeBase = Menu.AddNode(name)
	const StateNode = NodeBase.AddToggle("State", true)
	const AllyColor = NodeBase.AddColorPicker("Color ally", new Color(0, 255, 0, 66), "Color ally radius")
	const EnemyColor = NodeBase.AddColorPicker("Color enemy", new Color(255, 0, 0, 66), "Color enemy radius towers")
	const Width = NodeBase.AddSlider("Width radius", 15, 10, 75)
	const Sytle = NodeBase.AddDropdown("Style radius", ["Normal", "Rope", "Animation"])
	const Team = NodeBase.AddDropdown("Team", TeamArray)

	return {
		Team,
		Width,
		Sytle,
		NodeBase,
		StateNode,
		AllyColor,
		EnemyColor,
	}
}

// Custom radiuses
const CustomRadiusesMenu = Menu.AddNode("Custom")
CustomRadiusesMenu.sort_nodes = false
CustomRadiusesMenu.save_unused_configs = true
export interface CustomRadiusNode {
	BaseNode: IMenuParticlePicker
	range: MenuSDK.Slider
}
export let CustomRadiusesNodes: CustomRadiusNode[] = []
function OnCustomRadiusesChanged() {
	CustomRadiusesNodes.forEach(node => RADIUSES_CUSTOM.OnChanged(node, State.value))
}
const CustomRadiusesCountSlider = CustomRadiusesMenu.AddSlider("Count", 0, 0, 10)
function GetLocalizationForCustomRadiuses(): Map<string, string> {
	const map = new Map<string, string>()
	for (let i = 0; i < CustomRadiusesCountSlider.max; i++)
		map.set(`Radius #${i + 1}`, `Радиус #${i + 1}`)
	return map
}
MenuSDK.Localization.AddLocalizationUnit("russian", GetLocalizationForCustomRadiuses())
CustomRadiusesCountSlider.OnValue(({ value }) => {
	const CustomRadiusesCount = CustomRadiusesNodes.length
	if (CustomRadiusesCount < value) {
		for (let i = 0; i < value - CustomRadiusesCount; i++) {
			const visibleID = CustomRadiusesCount + i + 1
			const BaseNode = CustomRadiusesMenu.AddParticlePicker(
				`Radius #${visibleID}`,
				Color.Green,
				PARTICLE_NAME,
				[true, false],
			)
			BaseNode.Color.OnValue(OnCustomRadiusesChanged)
			BaseNode.State!.OnValue(OnCustomRadiusesChanged)
			BaseNode.Style.OnValue(OnCustomRadiusesChanged)
			BaseNode.Width.OnValue(OnCustomRadiusesChanged)
			const range = BaseNode.Node.AddSlider("Radius", 1200 * (visibleID / 1.5), 1, 5000)
			range.OnValue(OnCustomRadiusesChanged)
			CustomRadiusesNodes.push({ BaseNode, range })
		}
	} else {
		const removed = CustomRadiusesNodes.splice(value, CustomRadiusesCount - value)
		removed.forEach(node => {
			RADIUSES_CUSTOM.OnChanged(node, false)
			node.BaseNode.Node.DetachFromParent()
		})
	}
	OnCustomRadiusesChanged()
	CustomRadiusesCountSlider.IgnoreNextConfigLoad = true
})

// Heroes
export const CreateMenuRadiusHero = (heroName: string) => {
	const NodeBase = AbilitiesTree.AddNode(heroName, PathX.Heroes(heroName))
	const Selector = NodeBase.AddImageSelector("RADIUSES_X", [])
	return {
		NodeBase,
		Selector,
	}
}

// Heroes
export const AbilitiesTree = Menu.AddNode("Heroes")
AbilitiesTree.save_unused_configs = true

// Towers
export const Towers = CreateMenuTowerOrOutpost("Towers")
const TowerAttackTree = Towers.NodeBase.AddNode("Attack")
export const TowersAttackState = TowerAttackTree.AddToggle("State", true, "Turn on/off attack target tower")
export const TowersAttackColor = TowerAttackTree.AddColorPicker("Color", Color.Red.SetA(50))
export const TowersAttackTeamState = TowerAttackTree.AddDropdown("Team", TeamArray)

// Outposts
export const Outposts = CreateMenuTowerOrOutpost("Outposts")
