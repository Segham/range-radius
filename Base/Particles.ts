import { GameState, Menu, Outpost, ParticleAttachment_t, Team, Tower } from "wrapper/Imports"
import { BaseMenuSDKBulding, RADIUSES_DATA } from "../data"
import { State } from "../menu"

export class RADIUSES_PARTICLE_SERVICE {

	public static OnStateChanged(menu: BaseMenuSDKBulding, entity: (Tower | Outpost)[]) {
		const menuState = State.value && menu.StateNode.value
		entity.forEach(ent =>  {
			const name = this.Name(ent)
			if (menuState && !this.Team(menu.Team, ent))
				this.DrawOrUpdate(name, menu, ent)
			else
				RADIUSES_DATA.ParticlesSDK.DestroyByKey(name)
		})
	}

	public static Team(TeamSetting: Menu.Dropdown, ent: Tower | Outpost) {
		return GameState.LocalTeam !== Team.Observer && (
			(TeamSetting.selected_id === 1 && !ent.IsEnemy())
			|| (TeamSetting.selected_id === 2 && ent.IsEnemy())
		)
	}

	public static AttackTargetDestroy(entity: Tower[]) {
		entity.forEach(tower => RADIUSES_DATA.ParticlesSDK.DestroyByKey(RADIUSES_DATA.AttackName(tower)))
	}

	private static DrawOrUpdate(name: string, menu: BaseMenuSDKBulding, ent: Tower | Outpost) {
		const range = ent instanceof Tower ? ent.AttackRangeBonus() + 25 : ent.DayVision
		const color = ent.IsEnemy()
			? menu.EnemyColor.selected_color
			: menu.AllyColor.selected_color
		RADIUSES_DATA.ParticlesSDK.DrawCircle(name, ent, range, {
			Attachment: ParticleAttachment_t.PATTACH_ABSORIGIN_FOLLOW,
			Width: menu.Width.value,
			RenderStyle: menu.Sytle.selected_id,
			Color: color,
			Alpha: color.a,
		})
	}

	private static Name = (ent: Tower | Outpost) =>
		RADIUSES_DATA.BuildingRadiusName(ent)
}
