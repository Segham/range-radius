import { EventsSDK, LocalPlayer } from "wrapper/Imports"
import { RADIUSES_TICK_ABILITIES, RADIUSES_TICK_TOWERS } from "../Modules/index"

EventsSDK.on("Tick", () => {
	if (LocalPlayer === undefined)
		return
	RADIUSES_TICK_TOWERS()
	RADIUSES_TICK_ABILITIES()
})
