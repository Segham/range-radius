import { Ability, EventsSDK, Hero, Outpost, Tower } from "wrapper/Imports"
import { RADIUSES_DATA } from "../data"
import { RADIUSES_ABILITY_UPDATED, RADIUSES_ENTITY_LIFSTATE, RADIUSES_HERO_CREATED, RADIUSES_OUTPOST_CHANGED, RADIUSES_OUTPOST_DESTROY, RADIUSES_TOWER_DESTROY, RADIUSES_TOWER_LIFSTATE } from "../Modules/index"

EventsSDK.on("EntityTeamChanged", ent => {
	if (ent instanceof Outpost)
		RADIUSES_OUTPOST_CHANGED()
})

EventsSDK.on("EntityCreated", ent => {
	if (ent instanceof Hero)
		RADIUSES_HERO_CREATED(ent)

	if (ent instanceof Ability)
		RADIUSES_ABILITY_UPDATED(ent)
})

EventsSDK.on("EntityDestroyed", ent => {
	if (ent instanceof Outpost)
		RADIUSES_OUTPOST_DESTROY(ent)

	if (ent instanceof Tower)
		RADIUSES_TOWER_DESTROY(ent)

	if (ent instanceof Ability) {
		RADIUSES_ABILITY_UPDATED(ent)
		RADIUSES_DATA.DisposeAbilities(ent)
	}

	if (ent instanceof Hero)
		RADIUSES_DATA.DisposeHeroAbilities(ent)
})

EventsSDK.on("LifeStateChanged", ent => {
	if (ent instanceof Hero)
		RADIUSES_ENTITY_LIFSTATE(ent)

	if (ent instanceof Tower)
		RADIUSES_TOWER_LIFSTATE(ent)
})
