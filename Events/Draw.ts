import { EventsSDK, LocalPlayer } from "wrapper/Imports"
import { RADIUSES_DRAW_ABILITIES } from "../Modules/index"

EventsSDK.on("Draw", () => {
	if (LocalPlayer === undefined)
		return
	RADIUSES_DRAW_ABILITIES()
})
